# Python 리스트 컴프리헨션: 구문과 예 <sup>[1](#footnote_1)</sup>

<font size="3">Python에서 간결한 방법으로 다른 반복 가능한 리스트을 만드는 데 리스트 컴프리헨션을 사용하는 방법을 알아본다.</font>

## 목차

1. [개요](./list-comprehensions.md#intro)
1. [리스트 컴프리헨션(list comprehension)이란?](./list-comprehensions.md#sec_02)
1. [리스트 컴프리헨션을 작성하는 방법](./list-comprehensions.md#sec_03)
1. [리스트 컴프리헨션의 예](./list-comprehensions.md#sec_04)
1. [리스트 컴프리헨션 사용의 이점](./list-comprehensions.md#sec_05)
1. [일반적인 실수와 함정](./list-comprehensions.md#sec_06)
1. [요약](./list-comprehensions.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 30 — Python List Comprehensions: Syntax and Examples](https://levelup.gitconnected.com/python-tutorial-30-python-list-comprehensions-syntax-and-examples-d9a58056ff09?sk=204d82fe240a5cab41ff31707c5e3488)를 편역하였다.
